#!/bin/bash

# This is a modified version of the script I use to update my blacklist on my ERL router
# and dnsmasq server.  I've modified it to be more generic and to only output a new
# list of hosts to block instead of also applying that list to other servers.
# In the future, I plan on rewriting this in either python or go
#
# Source: https://gitlab.com/justyns/adblock-hosts-updater

# List of URLs to download.  They should all follow use similar formats, or be modified
# to be in a similar format.
urls=(
"http://winhelp2002.mvps.org/hosts.txt"
"http://someonewhocares.org/hosts/zero/hosts"
"http://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&startdate[day]=&startdate[month]=&startdate[year]=&mimetype=plaintext&useip=0.0.0.0"
"https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts"  # contains hosts from some of the other lists added here
"https://ssl.bblck.me/blacklists/hosts-file.txt"
"https://raw.githubusercontent.com/FadeMind/hosts.extras/master/UncheckyAds/hosts"  # added 09/21/2017
"https://www.malwaredomainlist.com/hostslist/hosts.txt"  # added 09/21/2017
"https://raw.githubusercontent.com/azet12/KADhosts/master/KADhosts.txt"  # added 09/21/2017
"https://raw.githubusercontent.com/StevenBlack/hosts/master/data/StevenBlack/hosts"  # added 09/21/2017
"https://raw.githubusercontent.com/FadeMind/hosts.extras/master/SpotifyAds/hosts"  # added 09/21/2017
"https://raw.githubusercontent.com/mitchellkrogza/Badd-Boyz-Hosts/master/hosts"  # added 09/21/2017
"https://zeustracker.abuse.ch/blocklist.php?download=hostfile"  # added 09/21/2017 - blocks known zeus c&c servers
"https://adaway.org/hosts.txt"   # added 04/01/2018 from https://www.reddit.com/r/GalaxyS8/comments/7op73u/discussion_adhell_custom_host/
"https://jasonhill.co.uk/pfsense/ytadblock.txt"  # added on 2019/08/24 - blocks yt ads, looks like it's mostly covered by other lists though
# "https://dbl.oisd.nl"   # added 2019/08/24 - doesn't contain ip in first column / disabled due to size for now
# These were added on 2019/10/14 from https://firebog.net/
"https://hosts-file.net/grm.txt"
"https://reddestdream.github.io/Projects/MinimalHosts/etc/MinimalHostsBlocker/minimalhosts"
"https://raw.githubusercontent.com/StevenBlack/hosts/master/data/KADhosts/hosts"
"https://raw.githubusercontent.com/StevenBlack/hosts/master/data/add.Spam/hosts"
"https://v.firebog.net/hosts/static/w3kbl.txt"
"https://v.firebog.net/hosts/AdguardDNS.txt"
"https://raw.githubusercontent.com/anudeepND/blacklist/master/adservers.txt"
"https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt"
"https://hosts-file.net/ad_servers.txt"
"https://v.firebog.net/hosts/Easylist.txt"
"https://raw.githubusercontent.com/StevenBlack/hosts/master/data/UncheckyAds/hosts"
"https://www.squidblacklist.org/downloads/dg-ads.acl"
# Added 2020/10/25
"https://rescure.me/rescure_domain_blacklist.txt"
"https://mirror1.malwaredomains.com/files/justdomains"
"https://mirror1.malwaredomains.com/files/immortal_domains.txt"
"https://raw.githubusercontent.com/piwik/referrer-spam-blacklist/master/spammers.txt"
"https://gitlab.com/ZeroDot1/CoinBlockerLists/raw/master/list_browser.txt"
"https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.Spam/hosts"
"https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/hosts.txt"
"https://raw.githubusercontent.com/Akamaru/Pi-Hole-Lists/master/gamefake.txt"
"https://raw.githubusercontent.com/Akamaru/Pi-Hole-Lists/master/mobile.txt"
"https://phishing.army/download/phishing_army_blocklist.txt"
"https://raw.githubusercontent.com/Perflyst/PiHoleBlocklist/master/SmartTV.txt"
"https://raw.githubusercontent.com/Spam404/lists/master/main-blacklist.txt"
# Added 2020/11/04 from https://github.com/EnergizedProtection/block
# "https://block.energized.pro/basic/formats/hosts"  # this one is > 20mb, skipping for now
"https://block.energized.pro/blu/formats/hosts"  # Only ~7mb
# Added 2021/04/11 from https://github.com/MoralCode/pihole-antitelemetry - empty for now
"https://raw.githubusercontent.com/MoralCode/pihole-antitelemetry/main/telemetry-domains.txt"
# Added 2022/05/07
"https://s3.amazonaws.com/lists.disconnect.me/simple_malvertising.txt"
"https://v.firebog.net/hosts/Admiral.txt"
"https://raw.githubusercontent.com/DandelionSprout/adfilt/master/Alternate%20versions%20Anti-Malware%20List/AntiMalwareHosts.txt"
"https://osint.digitalside.it/Threat-Intel/lists/latestdomains.txt"
"https://v.firebog.net/hosts/Prigent-Crypto.txt"
"https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-malware.txt"
"https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.Risk/hosts"
"https://urlhaus.abuse.ch/downloads/hostfile/"
# Added 2023/11/17 from https://github.com/hagezi/dns-blocklists
"https://raw.githubusercontent.com/hagezi/dns-blocklists/main/domains/tif.txt"
"https://raw.githubusercontent.com/hagezi/dns-blocklists/main/hosts/multi.txt"
"https://raw.githubusercontent.com/hagezi/dns-blocklists/main/adblock/personal.txt"
)

# These hosts will be whitelisted
ignorehosts=(
.*spotify.*
\.localdomain$
analytics\.google\.com
android\.clients\.google\.com
apps\.skype\.com
cdn\.mxpnl\.com
cdn\.optimizely\.com
client-s\.gateway\.messenger\.live\.com
clientconfig\.passport\.net
clients.*\.google\.com
dl\.dropbox\.com
events\.data\.microsoft\.com
g\.live\.com
google-analytics.com
googleadservices.com
googleapis\.com
google\.com
www\.google\.com
kerityn\.com
keystone\.mwbsys\.com
linksynergy.com
localhost$
m\.hotmail\.com
media1\.ancestry\.com
mixpanel.com
newrelic\.com
nr-data\.net
ojrq\.net
pastebin
po\.st
pricelist\.skype\.com
s3\.amazonaws\.com
s\.gateway\.messenger\.live\.com
s\.shopify.com
sa\.symcb\.com
shareasale\.com
sharesale\.com
spotify\.com
s{1..5}\.symcb\.com
tkqlhce\.com
tms\.capitalone\.com
trackcmp\.net
tracking\.epicgames\.com
ui\.skype\.com
upload.facebook.com creative.ak.fbcdn.net external-lhr0-1.xx.fbcdn.net external-lhr1-1.xx.fbcdn.net external-lhr10-1.xx.fbcdn.net external-lhr2-1.xx.fbcdn.net external-lhr3-1.xx.fbcdn.net external-lhr4-1.xx.fbcdn.net external-lhr5-1.xx.fbcdn.net external-lhr6-1.xx.fbcdn.net external-lhr7-1.xx.fbcdn.net external-lhr8-1.xx.fbcdn.net external-lhr9-1.xx.fbcdn.net fbcdn-creative-a.akamaihd.net scontent-lhr3-1.xx.fbcdn.net scontent.xx.fbcdn.net scontent.fgdl5-1.fna.fbcdn.net graph.facebook.com b-graph.facebook.com connect.facebook.com cdn.fbsbx.com api.facebook.com edge-mqtt.facebook.com mqtt.c10r.facebook.com portal.fb.com star.c10r.facebook.com star-mini.c10r.facebook.com b-api.facebook.com fb.me bigzipfiles.facebook.com
video-stats\.l\.google\.com
virtualearth\.net
ws-na\.amazon-adsystem\.com
www\.msftncsi\.com
xboxlive\.com
youtube\.com
i\.instagram\.com
amp-api-edge\.apps\.apple\.com
sf-api-token-service\.itunes\.apple\.com
ax.phobos.apple.com.edgesuite.net
dscx\.akamaiedge\.net
wac\.phicdn\.net
\.apple\.com$
)

# Explicit hosts to blacklist
blacklist="
0.0.0.0 cp.abbp1.space
"

IFS=\| eval 'whitelist="${ignorehosts[*]}"'
newhostsfile=newhosts.txt
outfile=hosts.new

echo "whitelist: ${whitelist}"
echo "# Hosts file generated at $(date)" > ${outfile}
echo "${blacklist}" > ${newhostsfile}

for url in "${urls[@]}"; do
    echo "Downloading ${url}.."
    # TODO: This could be cleaned up so that it's not using so many seds
    wget -qO- "${url}" \
        | sed 's/127.0.0.1/0.0.0.0/g' \
        | egrep -v '^#|^\s\+|^$' \
        | sed 's/0.0.0.0/ /g' \
        | sed 's/#.*$//g' \
        | sed 's/\s\+//g' \
        | egrep -v '^#|^\s\+|^$|^\s\+$' \
        | sed 's/^/0.0.0.0 /g' \
        | sed $'s/\r$//' \
        | sort >> ${newhostsfile}
done

echo "Removing whitelisted hosts.."
cat ${newhostsfile} | egrep -vi "${whitelist}" > ${newhostsfile}2

echo "Sorting and deduping.."
cat ${newhostsfile}2 | sort -u >> ${outfile}
ls -lah ${newhostsfile} ${outfile} hosts.txt
echo "Diff between previous and current version"
diff hosts.txt ${outfile} > hosts.diff
echo ""
echo "Total hosts: $(wc -l ${newhostsfile}2)"
echo "Whitelisted hosts: $(cat ${newhostsfile} | egrep -ic \"${whitelist}\")"
echo "Total unique hosts: $(wc -l ${outfile})"

# Cleanup
mv -v ${outfile} hosts.txt
rm -fv ${newhostsfile}*
