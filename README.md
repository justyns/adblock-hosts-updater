# adblock-hosts-updater

Script to update a hosts file for use by dnsmasq or some other dns-based ad blocker.

## Download

To use these host lists, you can download the raw hosts file from one of the below locations.

- https://gitlab.com/justyns/adblock-hosts-updater/-/raw/master/hosts.txt?inline=false
- https://gist.githubusercontent.com/justyns/1cc784a67ffb74ffd4adf19655bab2b7/raw/d770d53d258e8b6bb2cec1f3cd12e047aced3385/hosts.dnsmasq (not updated as frequently)
