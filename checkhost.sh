#!/usr/bin/env bash
# set -euo pipefail

# Checks a single host

host=$1

# List of resolvers so we don't send all requests to a single one
# Google
resolvers[0]="8.8.8.8"
resolvers[1]="8.8.4.4"
# OpenDNS
resolvers[2]="208.67.222.222"
resolvers[3]="208.67.220.220"
# Cloudflare
resolvers[4]="1.0.0.1"
resolvers[5]="1.1.1.1"
# Quad9
resolvers[6]="9.9.9.9"
resolvers[7]="149.112.112.112"

rand=$(( RANDOM % 7 ))
resolver=${resolvers[$rand]}

# echo -en "\r\033[${pos}K Testing $host.."
res="$(dig +short @$resolver "${host}" 2> dig.err)"
if [ -z "$host" ] || [ -z "$res" ]; then
    echo -e "\r\033[K $host does not resolve via $resolver."
    echo "0.0.0.0 $host  # $(cat dig.err)" >> hosts.dead.txt
else
    echo "0.0.0.0 $host" >> hosts.alive.txt
    echo "$host" >> domains.txt
fi
