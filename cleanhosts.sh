#!/usr/bin/env bash
# set -euo pipefail

# This script takes the output of updateadblockhosts.sh and attempts to clean/optimize it by:
#   - removing hosts that don't resolve
#   - TODO: removing unregistered domains
#   - removing invalid hostnames


echo "# Hosts file generated at $(date)" > hosts.dead.txt
echo "# Hosts file generated at $(date)" > hosts.alive.txt
echo "# Hosts file generated at $(date)" > domains.txt
rm -f dig.err

# cat hosts.txt | egrep -v '^#|^\s|^$' | while read ip host;do
#     echo -en "\r\033[K Testing $host.."
#     res="$(dig +short @8.8.8.8 $host 2> dig.err)"
#     if [ -z "$host" ] || [ -z "$res" ]; then
#         echo "$host does not resolve."
#         echo "0.0.0.0 $host  # $(cat dig.err)" >> hosts.dead.txt
#     else
#         echo "0.0.0.0 $host" >> hosts.alive.txt
#     fi
# done

# Use parallel to make things a little faster
cat hosts.txt | egrep -v '^#|^\s|^$' | while read ip host;do echo $host;done | parallel -j 200% ./checkhost.sh "{}" {%}

mv hosts.alive.txt hosts.txt
